# Changelog

## [0.3.0] - 2024-06-30

### Features

- issues: Assign security team when scope::security is set #4

### Miscellaneous Tasks

- doc: Correct a typo in README.md
- deps: Update dependencies

## [0.2.0] - 2023-11-28

### Features

- label: Assign default labels for severity and priority
- merge_requests: Assign merge requests to package maintainers

### Miscellaneous Tasks

- doc: Fix some typos in the README.md

## [0.1.4] - 2023-11-17

### Bug Fixes

- bot: Lower packager names from ArchWeb to match keycloak
- cliff: Update the tag_pattern to use regex

### Miscellaneous Tasks

- clippy: Avoid redundant as_str

## [0.1.3] - 2023-10-16

### Features

- bot: Use async stream processing for querying GitLab entries
- daemon: Wait for a fixed delay between full runs

### Miscellaneous Tasks

- deps: Update dependencies

## [0.1.2] - 2023-10-14

### Features

- daemon: Delay first data update as we start with fresh data
- daemon: Reduce read/write lock to data struct

### Miscellaneous Tasks

- bot: Split struct fields into concurrency, handle and data

## [0.1.1] - 2023-10-13

### Features

- options: Allow configuring behavior with options and env vars #3
- daemon: Implement full run mode on fixed interval #2

### Miscellaneous Tasks

- log: Lower some logging level for less noisy output

## [0.1.0] - 2023-10-05

### Bug Fixes

- daemon: Bind to all interface types
- note: Declare appropriate lifetime for note tokens
- error: Fix ignored error handling of collected futures
- bot: Limit concurrent requests to avoid running out of resources
- bot: Pass issue iid when querying project issues
- webhook: Use object_kind to also handle confidential event types

### Documentation

- readme: Add initial version describing the scope and functionality
- readme: Show example that commands can contain comments

### Features

- commands: Add architecture for custom commands like assign
- resolution: Add code path to auto label closed issues
- daemon: Add daemon that can listen to GitLab webhook calls
- ci: Add gitlab CI with build, test and checks
- webhook: Add shared secret for the GitLab webhook API
- unconfirmed: Allow to opt-in to get assigned to unconfirmed issues
- status: Assign unconfirmed issues to bug wranglers
- bugbuddy: Basic functionality to assign based on archweb
- notes: Collect additional assignees from issue notes
- bot: Only assign package maintainers to confirmed issues
- notes: Only consider notes written by staff users
- merge-request: Prepare axum to handle merge request webhooks
- notes: Process issue notes for none maintainer assignments
- daemon: Refresh maintainer and staff data periodically

### Miscellaneous Tasks

- license: Add MIT license
- changelog: Add a config file for git cliff
- cargo: Add more metadata, repository and license
- make: Adding Makefile to ease release process
- cleanup: Make cargo happy
- cleanup: Make clippy happy like a hippo
- cleanup: Rename label category to scope to match upstream
- cleanup: Restructure each step and use a plan struct
- format: Run cargo fmt
- cleanup: Simplify issue note tokenizing
- deps: Upgrade all dependencies
- deps: Upgrade all dependencies
- cleanup: Use Self instead of explicit type name


