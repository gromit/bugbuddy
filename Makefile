CARGO := cargo
INSTALL := install
SED := sed
GIT := git
GPG := gpg

PROJECT=bugbuddy
TARBALLDIR ?= target/release/tarball

DEBUG := 0
ifeq ($(DEBUG), 0)
	CARGO_OPTIONS := --release --locked
else
	CARGO_OPTIONS :=
endif

.PHONY: all $(PROJECT) test lint release

all: $(PROJECT) test lint

$(PROJECT):
	$(CARGO) build $(CARGO_OPTIONS)

test:
	$(CARGO) test $(CARGO_OPTIONS)

lint:
	$(CARGO) fmt -- --check
	$(CARGO) check
	$(CARGO) clippy --all -- -D warnings


release: all
	$(INSTALL) -d $(TARBALLDIR)
	$(GIT) cliff --strip=all --unreleased
	@glab --version &>/dev/null
	@$(CARGO) pkgid | $(SED) 's/.*#/current version: /'
	@read -p 'version> ' VERSION && \
		$(SED) -E "s|^version = .*|version = \"$$VERSION\"|" -i Cargo.toml && \
		$(CARGO) build --release && \
		$(GIT) cliff --tag "v$$VERSION" > CHANGELOG.md && \
		$(GIT) commit --gpg-sign --message "chore(release): version v$$VERSION" Cargo.toml Cargo.lock CHANGELOG.md && \
		$(GIT) tag --sign --message "Version v$$VERSION" v$$VERSION && \
		$(GIT) archive --format tar --prefix=$(PROJECT)-v$$VERSION/ v$$VERSION | gzip -cn > $(TARBALLDIR)/$(PROJECT)-v$$VERSION.tar.gz && \
		$(GPG) --detach-sign $(TARBALLDIR)/$(PROJECT)-v$$VERSION.tar.gz && \
		$(GPG) --detach-sign --yes target/release/$(PROJECT) && \
		$(GIT) push --tags origin main && \
		glab release create v$$VERSION $(TARBALLDIR)/$(PROJECT)-v$$VERSION.tar.gz* target/release/$(PROJECT) target/release/$(PROJECT).sig --milestone v$$VERSION --notes-file <(git cliff --strip=all --latest)
