pub mod args;
pub mod daemon;
pub mod types;

use crate::args::Args;
use crate::types::{
    AwardEmoji, GitLabUser, GitLabUsers, GroupProjects, LabelScope, Note, Packager,
    PkgbaseMaintainers, Plan, PlanState, ProjectIssue, ProjectMergeRequest, ResolutionLabel,
    ScopeLabel, StatusLabel,
};
use std::collections::HashSet;
use std::env;
use std::ops::Add;
use std::sync::Arc;

use gitlab::api::groups::projects::GroupProjectsOrderBy;
use gitlab::api::issues::IssueState;
use gitlab::api::{AsyncQuery, Paged};
use gitlab::{AsyncGitlab, GitlabBuilder};

use anyhow::{Context, Result};
use futures::future::try_join_all;
use futures::pin_mut;
use futures::stream::StreamExt;
use gitlab::api::common::SortOrder;
use gitlab::api::projects::issues::notes::NoteOrderBy;
use gitlab::api::projects::merge_requests::MergeRequestState;
use log::{debug, error, info, trace};
use reqwest::Client;
use serde::de::DeserializeOwned;
use time::{Duration, OffsetDateTime};
use tokio::sync::{RwLock, Semaphore};

const STAFF_GROUP: &str = "archlinux/teams/staff";
const PACKAGE_MAINTAINER: &str = "archlinux/teams/package-maintainer-team/package-maintainers";
const JUNIOR_PACKAGE_MAINTAINER: &str =
    "archlinux/teams/package-maintainer-team/junior-package-maintainers";
const CORE_PACKAGE_MAINTAINER: &str =
    "archlinux/teams/package-maintainer-team/core-package-maintainers";
const JUNIOR_CORE_PACKAGE_MAINTAINER: &str =
    "archlinux/teams/package-maintainer-team/junior-core-package-maintainers";
const BUG_WRANGLER_GROUP: &str = "archlinux/teams/bug-wranglers";

const SECURITY_TEAM_GROUP: &str = "archlinux/security-team";

const MARK_CLOSED_AS_COMPLETED_AFTER: Duration = Duration::minutes(5);

const EMOJI_ASSIGN_UNCONFIRMED_TO_PACKAGE_MAINTAINER: &str = "mag";

#[derive(Clone)]
pub struct BugBuddy {
    concurrency: BotConcurrency,
    handle: BotHandle,
    data: Arc<RwLock<BotData>>,
}

#[derive(Clone)]
pub struct BotConcurrency {
    project: Arc<Semaphore>,
    issue: Arc<Semaphore>,
}

#[derive(Clone)]
pub struct BotHandle {
    args: Args,
    gitlab_token: String,
    client: AsyncGitlab,
    bot_user: GitLabUser,
}

#[derive(Clone)]
pub struct BotData {
    maintainers: PkgbaseMaintainers,
    gitlab_users: GitLabUsers,
    bug_wranglers: GitLabUsers,
    security_team_members: GitLabUsers,
    unconfirmed_active: HashSet<Packager>,
}

impl BotData {
    pub async fn new(handle: &BotHandle) -> Result<Self> {
        let mut data = Self {
            maintainers: PkgbaseMaintainers::new(),
            gitlab_users: GitLabUsers::new(),
            bug_wranglers: GitLabUsers::new(),
            security_team_members: GitLabUsers::new(),
            unconfirmed_active: HashSet::new(),
        };
        data.update_data(handle).await?;
        Ok(data)
    }

    pub async fn update_data(&mut self, handle: &BotHandle) -> Result<()> {
        info!("updating packages, maintainer and staff data...");
        let maintainers = self.query_maintainers(handle).await?;

        let mut gitlab_users = GitLabUsers::new();
        let mut bug_wranglers = GitLabUsers::new();
        let mut security_team_members = GitLabUsers::new();

        Self::query_gitlab_users(&handle.client, STAFF_GROUP, &mut gitlab_users).await?;
        Self::query_gitlab_users(&handle.client, PACKAGE_MAINTAINER, &mut gitlab_users).await?;
        Self::query_gitlab_users(&handle.client, JUNIOR_PACKAGE_MAINTAINER, &mut gitlab_users)
            .await?;
        Self::query_gitlab_users(&handle.client, CORE_PACKAGE_MAINTAINER, &mut gitlab_users)
            .await?;
        Self::query_gitlab_users(
            &handle.client,
            JUNIOR_CORE_PACKAGE_MAINTAINER,
            &mut gitlab_users,
        )
        .await?;
        Self::query_gitlab_users(&handle.client, BUG_WRANGLER_GROUP, &mut bug_wranglers).await?;
        Self::query_gitlab_users(
            &handle.client,
            SECURITY_TEAM_GROUP,
            &mut security_team_members,
        )
        .await?;

        self.maintainers = maintainers;
        self.gitlab_users = gitlab_users;
        self.bug_wranglers = bug_wranglers;
        self.security_team_members = security_team_members;
        self.update_per_user_settings(handle).await?;

        Ok(())
    }

    pub async fn update_per_user_settings(&mut self, handle: &BotHandle) -> Result<()> {
        let url = format!(
            "{}/api/v4/projects/{}/issues/{}/award_emoji",
            handle.args.gitlab_url, handle.args.control_project_id, handle.args.control_issue_iid
        );
        let response = Client::new()
            .get(url)
            .bearer_auth(&handle.gitlab_token)
            .send()
            .await?;
        let result: Vec<AwardEmoji> = serde_json::from_str(response.text().await?.as_str())?;
        let result: HashSet<_> = result
            .into_iter()
            .filter(|emoji| {
                emoji
                    .name
                    .eq(EMOJI_ASSIGN_UNCONFIRMED_TO_PACKAGE_MAINTAINER)
            })
            .map(|emoji| emoji.user.username)
            .collect();

        self.unconfirmed_active = result;
        Ok(())
    }

    pub async fn query_maintainers(&self, handle: &BotHandle) -> Result<PkgbaseMaintainers> {
        let response = Client::new()
            .get(&handle.args.pkgbase_maintainer_url.to_string())
            .send()
            .await?;
        let mut maintainers: PkgbaseMaintainers =
            serde_json::from_str(response.text().await?.as_str())?;
        for entry in maintainers.iter_mut() {
            for packagers in entry.1.iter_mut().enumerate() {
                *packagers.1 = packagers.1.to_lowercase();
            }
        }
        trace!("pkgbase maintainers: {:?}", maintainers);
        Ok(maintainers)
    }

    pub async fn query_gitlab_users(
        client: &AsyncGitlab,
        group: &str,
        users: &mut GitLabUsers,
    ) -> Result<()> {
        let endpoint = gitlab::api::groups::members::GroupMembers::builder()
            .group(group)
            .build()
            .unwrap();
        let members: Vec<GitLabUser> = gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
            .query_async(client)
            .await?;
        for member in members {
            debug!("member {:?}", member);
            users.insert(member.username.to_string(), member);
        }
        Ok(())
    }
}

impl BugBuddy {
    pub async fn new(args: Args) -> Result<Self> {
        let gitlab_token =
            env::var("BUGBUDDY_GITLAB_TOKEN").context("Missing env var BUGBUDDY_GITLAB_TOKEN")?;

        let client =
            GitlabBuilder::new(&args.gitlab_url.host().unwrap().to_string(), &gitlab_token)
                .build_async()
                .await?;

        let current_user = Self::query_gitlab_current_user(&client).await?;

        let handle = BotHandle {
            args: args.clone(),
            gitlab_token,
            client,
            bot_user: current_user,
        };

        let data = BotData::new(&handle).await?;
        let data = Arc::new(RwLock::new(data));

        let concurrency = BotConcurrency {
            project: Arc::new(Semaphore::new(args.concurrent_projects)),
            issue: Arc::new(Semaphore::new(args.concurrent_issues)),
        };

        Ok(Self {
            concurrency,
            handle,
            data,
        })
    }

    pub async fn update_data(&self) -> Result<()> {
        self.data.write().await.update_data(&self.handle).await
    }

    pub async fn query_gitlab_current_user(client: &AsyncGitlab) -> Result<GitLabUser> {
        let endpoint = gitlab::api::users::CurrentUser::builder().build().unwrap();
        let user: GitLabUser = endpoint.query_async(client).await?;
        Ok(user)
    }

    pub async fn run(&self, arc: &Arc<Self>) -> Result<()> {
        info!(
            "Starting full run on group {}",
            &self.handle.args.gitlab_packages_group
        );

        let projects = self.get_group_projects_paged(&self.handle.args.gitlab_packages_group);
        let projects_stream = projects.iter_async(&self.handle.client);
        pin_mut!(projects_stream);

        let mut join_handles = Vec::new();
        while let Some(project) = projects_stream.next().await {
            let project: GroupProjects = project?;

            let arc = arc.clone();
            let semaphore = self.concurrency.project.clone();
            join_handles.push(tokio::spawn(async move {
                let permit = semaphore.acquire_owned().await?;
                let result = arc.process_project(&arc, &project).await;
                drop(permit);
                if let Err(err) = &result {
                    error!("Error processing project {}", project.name);
                    for cause in err.chain() {
                        error!("Caused by: {:?}", cause)
                    }
                }
                result
            }));
        }

        try_join_all(join_handles).await?;
        info!(
            "Finished full run on group {}",
            &self.handle.args.gitlab_packages_group
        );
        Ok(())
    }

    pub fn get_group_projects_paged<'a>(
        &'a self,
        group_path: &'a str,
    ) -> Paged<gitlab::api::groups::projects::GroupProjects> {
        let endpoint = gitlab::api::groups::projects::GroupProjects::builder()
            .group(group_path)
            .archived(false)
            .order_by(GroupProjectsOrderBy::Name)
            .sort(SortOrder::Ascending)
            .build()
            .unwrap();
        gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
    }

    pub async fn get_group_projects(&self, group_path: &str) -> Result<Vec<GroupProjects>> {
        let endpoint = gitlab::api::groups::projects::GroupProjects::builder()
            .group(group_path)
            .archived(false)
            .order_by(GroupProjectsOrderBy::LastActivityAt)
            .build()
            .unwrap();

        let projects: Vec<GroupProjects> =
            gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
                .query_async(&self.handle.client)
                .await?;
        Ok(projects)
    }

    pub async fn get_project(&self, project_id: u64) -> Result<GroupProjects> {
        let endpoint = gitlab::api::projects::Project::builder()
            .project(project_id)
            .build()
            .unwrap();

        let project: GroupProjects = endpoint.query_async(&self.handle.client).await?;
        Ok(project)
    }

    pub async fn get_project_issue(&self, project_id: u64, issue_iid: u64) -> Result<ProjectIssue> {
        let endpoint = gitlab::api::projects::issues::Issue::builder()
            .project(project_id)
            .issue(issue_iid)
            .build()
            .unwrap();

        let issue: ProjectIssue = endpoint.query_async(&self.handle.client).await?;
        Ok(issue)
    }

    pub async fn get_project_merge_request(
        &self,
        project_id: u64,
        merge_request_iid: u64,
    ) -> Result<ProjectMergeRequest> {
        let endpoint = gitlab::api::projects::merge_requests::MergeRequest::builder()
            .project(project_id)
            .merge_request(merge_request_iid)
            .build()
            .unwrap();

        let merge_request: ProjectMergeRequest = endpoint.query_async(&self.handle.client).await?;
        Ok(merge_request)
    }

    pub async fn process_project(
        &self,
        arc: &Arc<BugBuddy>,
        project: &GroupProjects,
    ) -> Result<()> {
        info!(
            "Wrangling bugs for project {} ({})",
            project.name_with_namespace, project.name
        );

        // TODO: this only selects opened issues
        let endpoint = gitlab::api::projects::issues::Issues::builder()
            .project(project.path_with_namespace.to_string())
            .state(IssueState::Opened)
            .build()
            .unwrap();

        let issues = gitlab::api::paged(endpoint, gitlab::api::Pagination::All);
        let issues_stream = issues.iter_async(&self.handle.client);
        pin_mut!(issues_stream);

        let mut join_handles = Vec::new();
        while let Some(issue) = issues_stream.next().await {
            let issue: ProjectIssue = issue?;
            let bot = arc.clone();
            let semaphore = self.concurrency.issue.clone();
            let project_cloned: GroupProjects = project.clone();
            join_handles.push(tokio::spawn(async move {
                let permit = semaphore.acquire_owned().await?;
                let result = bot.process_issue(&project_cloned, &issue).await;
                if let Err(err) = &result {
                    error!(
                        "Error processing issue {} of project {}",
                        issue.iid, project_cloned.name
                    );
                    for cause in err.chain() {
                        error!("Caused by: {:?}", cause)
                    }
                }
                drop(permit);
                result
            }));
        }

        let merge_request_endpoint =
            gitlab::api::projects::merge_requests::MergeRequests::builder()
                .project(project.path_with_namespace.to_string())
                .state(MergeRequestState::Opened)
                .build()
                .unwrap();

        let merge_requests =
            gitlab::api::paged(merge_request_endpoint, gitlab::api::Pagination::All);
        let merge_request_stream = merge_requests.iter_async(&self.handle.client);
        pin_mut!(merge_request_stream);

        while let Some(merge_request) = merge_request_stream.next().await {
            let merge_request: ProjectMergeRequest = merge_request?;
            let bot = arc.clone();
            let semaphore = self.concurrency.issue.clone();
            let project_cloned: GroupProjects = project.clone();
            join_handles.push(tokio::spawn(async move {
                let permit = semaphore.acquire_owned().await?;
                let result = bot
                    .process_merge_request(&project_cloned, &merge_request)
                    .await;
                if let Err(err) = &result {
                    error!(
                        "Error processing merge_request {} of project {}",
                        merge_request.iid, project_cloned.name
                    );
                    for cause in err.chain() {
                        error!("Caused by: {:?}", cause)
                    }
                }
                drop(permit);
                result
            }));
        }

        try_join_all(join_handles).await?;
        Ok(())
    }

    pub async fn process_issue_id(&self, project_id: u64, issue_iid: u64) -> Result<()> {
        // TODO: acquite semaphpre here?
        let project = self.get_project(project_id).await?;
        let issue = self.get_project_issue(project.id, issue_iid).await?;
        self.process_issue(&project, &issue).await
    }

    pub async fn process_merge_request_id(
        &self,
        project_id: u64,
        merge_request_iid: u64,
    ) -> Result<()> {
        let project = self.get_project(project_id).await?;
        let merge_request = self
            .get_project_merge_request(project.id, merge_request_iid)
            .await?;
        self.process_merge_request(&project, &merge_request).await
    }

    pub async fn process_issue_id_logged(&self, project_id: u64, issue_iid: u64) -> Result<()> {
        let result = self.process_issue_id(project_id, issue_iid).await;
        if let Err(err) = &result {
            error!(
                "Error processing issue {} for project {}: {:?}",
                issue_iid, project_id, err
            );
            for cause in err.chain() {
                error!("Caused by: {:?}", cause)
            }
        }
        result
    }

    pub async fn process_merge_request_id_logged(
        &self,
        project_id: u64,
        merge_request_iid: u64,
    ) -> Result<()> {
        let result = self
            .process_merge_request_id(project_id, merge_request_iid)
            .await;
        if let Err(err) = &result {
            error!(
                "Error processing merge_request {} for project {}: {:?}",
                merge_request_iid, project_id, err
            );
            for cause in err.chain() {
                error!("Caused by: {:?}", cause)
            }
        }
        result
    }

    pub async fn process_issue(&self, project: &GroupProjects, issue: &ProjectIssue) -> Result<()> {
        info!(
            "project: {} issue: {} id: {} iid: {} labels: {:?} assignees: {:?}",
            project.name, issue.title, issue.id, issue.iid, issue.labels, issue.assignees
        );

        let mut plan = Plan::new(issue.iid, PlanState::Issue(issue.state));

        self.process_issue_labels(&mut plan, project, issue).await?;
        self.process_issue_notes(&mut plan, project, issue).await?;
        self.process_package_maintainers(&mut plan, project).await?;
        self.maybe_add_bug_wranglers(&mut plan).await?;
        self.maybe_add_security_team_members(&mut plan).await?;

        self.maybe_execute_plan(&plan, project, issue).await?;

        Ok(())
    }

    pub async fn process_merge_request(
        &self,
        project: &GroupProjects,
        merge_request: &ProjectMergeRequest,
    ) -> Result<()> {
        info!(
            "project: {} merge_request: {} id: {} iid: {} labels: {:?} assignees: {:?}",
            project.name,
            merge_request.title,
            merge_request.id,
            merge_request.iid,
            merge_request.labels,
            merge_request.assignees
        );

        let mut plan = Plan::new(
            merge_request.iid,
            PlanState::MergeRequest(merge_request.state),
        );

        self.process_merge_request_labels(&mut plan, project, merge_request)
            .await?;
        self.process_merge_request_notes(&mut plan, project, merge_request)
            .await?;
        self.process_merge_request_package_maintainers(&mut plan, project)
            .await?;

        self.maybe_execute_merge_request_plan(&plan, project, merge_request)
            .await?;

        Ok(())
    }

    pub async fn maybe_add_bug_wranglers(&self, plan: &mut Plan) -> Result<()> {
        // only assign bug wranglers to unconfirmed issues
        if !plan.is_unconfirmed {
            return Ok(());
        }

        // add bug wranglers to assignees
        plan.assignees.extend(
            self.data
                .read()
                .await
                .bug_wranglers
                .values()
                .map(|user| user.username.to_string()),
        );

        Ok(())
    }

    pub async fn maybe_add_security_team_members(&self, plan: &mut Plan) -> Result<()> {
        // only assign security members to issues relevant to them
        if !plan.is_security {
            return Ok(());
        }

        // add security team members to assignees as its a security issue
        plan.assignees.extend(
            self.data
                .read()
                .await
                .security_team_members
                .values()
                .map(|user| user.username.to_string()),
        );

        Ok(())
    }

    pub async fn is_staff_user(&self, username: &str) -> bool {
        self.data.read().await.gitlab_users.contains_key(username)
    }

    pub fn issue_has_status_label(&self, issue: &ProjectIssue, status: StatusLabel) -> bool {
        issue
            .labels
            .iter()
            .any(|label| label.eq(status.as_label_name()))
    }

    pub fn issue_has_scope_label(&self, issue: &ProjectIssue, scope: ScopeLabel) -> bool {
        issue
            .labels
            .iter()
            .any(|label| label.eq(scope.as_label_name()))
    }

    pub fn issue_status_is_unconfirmed(&self, issue: &ProjectIssue) -> bool {
        !self.issue_has_label_scope(issue, LabelScope::Status)
            || self.issue_has_status_label(issue, StatusLabel::Unconfirmed)
    }

    pub fn issue_scope_is_security(&self, issue: &ProjectIssue) -> bool {
        !self.issue_has_label_scope(issue, LabelScope::Scope)
            || self.issue_has_scope_label(issue, ScopeLabel::Security)
    }

    pub fn issue_has_label_scope(&self, issue: &ProjectIssue, category: LabelScope) -> bool {
        issue
            .labels
            .iter()
            .any(|label| label.starts_with::<&str>(category.into()))
    }

    pub fn merge_request_status_is_unconfirmed(&self, merge_request: &ProjectMergeRequest) -> bool {
        !self.merge_request_has_label_scope(merge_request, LabelScope::Status)
            || self.merge_request_has_status_label(merge_request, StatusLabel::Unconfirmed)
    }

    pub fn merge_request_has_label_scope(
        &self,
        merge_request: &ProjectMergeRequest,
        category: LabelScope,
    ) -> bool {
        merge_request
            .labels
            .iter()
            .any(|label| label.starts_with::<&str>(category.into()))
    }

    pub fn merge_request_has_status_label(
        &self,
        merge_request: &ProjectMergeRequest,
        status: StatusLabel,
    ) -> bool {
        merge_request
            .labels
            .iter()
            .any(|label| label.eq(status.as_label_name()))
    }

    pub async fn gitlab_users_from_usernames<'a, I>(&self, usernames: I) -> Vec<GitLabUser>
    where
        I: IntoIterator<Item = &'a String>,
    {
        let data = self.data.read().await;
        usernames
            .into_iter()
            .filter_map(move |packager| data.gitlab_users.get(packager.as_str()).cloned())
            .collect()
    }

    pub fn gitlab_user_ids_from_gitlab_users<'a, I>(&self, users: I) -> Vec<u64>
    where
        I: IntoIterator<Item = &'a GitLabUser>,
    {
        users.into_iter().map(|maintainer| maintainer.id).collect()
    }

    pub fn gitlab_username_from_gitlab_users<I>(&self, users: I) -> Vec<String>
    where
        I: IntoIterator<Item = GitLabUser>,
    {
        users
            .into_iter()
            .map(|maintainer| maintainer.username.clone())
            .collect()
    }

    pub async fn process_issue_labels(
        &self,
        plan: &mut Plan,
        project: &GroupProjects,
        issue: &ProjectIssue,
    ) -> Result<()> {
        // assign unconfirmed status
        if self.issue_status_is_unconfirmed(issue) {
            plan.is_unconfirmed = true;
        }

        // query whether its a security issue
        if self.issue_scope_is_security(issue) {
            plan.is_security = true;
        }

        self.assign_default_issue_label(plan, issue, LabelScope::Status)
            .await?;
        self.assign_default_issue_label(plan, issue, LabelScope::Severity)
            .await?;
        self.assign_default_issue_label(plan, issue, LabelScope::Priority)
            .await?;

        self.process_missing_issue_resolution_label(plan, project, issue)
            .await?;

        Ok(())
    }

    pub async fn assign_default_issue_label(
        &self,
        plan: &mut Plan,
        issue: &ProjectIssue,
        scope: LabelScope,
    ) -> Result<()> {
        // do nothing if issue already has a label of this scope
        if self.issue_has_label_scope(issue, scope) {
            return Ok(());
        }

        info!(
            "issue {} has no {:?} label assigned, assigning {}",
            issue.title,
            scope,
            scope.default(),
        );
        plan.labels.insert(scope.default().to_string());

        Ok(())
    }

    pub async fn process_missing_issue_resolution_label(
        &self,
        plan: &mut Plan,
        _project: &GroupProjects,
        issue: &ProjectIssue,
    ) -> Result<()> {
        // do nothing if issue not yet closed
        if types::IssueState::Closed != issue.state {
            return Ok(());
        }

        // do nothing if issue has a resolution label
        if self.issue_has_label_scope(issue, LabelScope::Resolution) {
            return Ok(());
        }

        // do nothing if wait time not reached to give maintainer time to assign a label
        if issue
            .closed_at
            .unwrap()
            .add(MARK_CLOSED_AS_COMPLETED_AFTER)
            .gt(&OffsetDateTime::now_utc())
        {
            return Ok(());
        }

        info!(
            "issue {} is closed without resolution, assigning {}",
            issue.title,
            ResolutionLabel::Completed.as_label_name()
        );
        plan.labels
            .insert(ResolutionLabel::Completed.as_label_name().to_string());

        Ok(())
    }

    pub async fn process_issue_notes<'a>(
        &'a self,
        plan: &mut Plan,
        project: &GroupProjects,
        issue: &ProjectIssue,
    ) -> Result<()> {
        let endpoint = gitlab::api::projects::issues::notes::IssueNotes::builder()
            .project(project.id)
            .issue(issue.iid)
            .order_by(NoteOrderBy::CreatedAt)
            .sort(SortOrder::Ascending)
            .build()
            .unwrap();
        let notes: Vec<Note> = gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
            .query_async(&self.handle.client)
            .await?;

        for note in notes {
            self.process_note(plan, project, &note).await?;
        }

        Ok(())
    }

    pub async fn gitlab_api_get<R: DeserializeOwned>(&self, url: &str) -> Result<R> {
        let response = Client::new()
            .get(url)
            .bearer_auth(&self.handle.gitlab_token)
            .send()
            .await?;
        let result: R = serde_json::from_str(response.text().await?.as_str())?;
        Ok(result)
    }

    pub async fn gitlab_api_post<R: DeserializeOwned>(&self, url: &str) -> Result<R> {
        let response = Client::new()
            .post(url)
            .bearer_auth(&self.handle.gitlab_token)
            .send()
            .await?;
        let result: R = serde_json::from_str(response.text().await?.as_str())?;
        Ok(result)
    }

    pub fn strip_at_sign_from_usernames<'g, I>(usernames: I) -> Vec<String>
    where
        I: IntoIterator<Item = &'g &'g str>,
    {
        usernames
            .into_iter()
            .filter(|s| s.starts_with('@'))
            .map(|s| s.replacen('@', "", 1).to_string())
            .collect()
    }

    pub async fn process_note<'a>(
        &'a self,
        plan: &mut Plan,
        project: &GroupProjects,
        note: &Note,
    ) -> Result<()> {
        // skip if not enough tokens
        let token: Vec<&str> = note
            .body
            .lines()
            .next()
            .unwrap_or_default()
            .split(' ')
            .filter(|s| !s.is_empty())
            .collect();
        trace!("token {:?}", token);
        if token.len() < 2 {
            return Ok(());
        }

        // skip notes that are not addressed to the bug bot
        let recipient = token[0];
        if !recipient.starts_with('@') || !self.handle.bot_user.username.eq(&recipient[1..]) {
            return Ok(());
        }

        let url = match plan.state {
            PlanState::Issue(_) =>
                format!(
                    "https://gitlab.archlinux.org/api/v4/projects/{}/issues/{}/notes/{}/award_emoji",
                    project.id, plan.iid, note.id),
            PlanState::MergeRequest(_) =>
                format!(
                    "https://gitlab.archlinux.org/api/v4/projects/{}/merge_requests/{}/notes/{}/award_emoji",
                    project.id, plan.iid, note.id),
        };
        let emojis: Vec<_> = self
            .gitlab_api_get::<Vec<AwardEmoji>>(&url)
            .await?
            .into_iter()
            .filter(|emoji| emoji.user.id == self.handle.bot_user.id)
            .collect();
        trace!("emoji: {:?}", emojis);

        let is_staff_user = self.is_staff_user(&note.author.username).await;
        let has_bugbot_emoji = !emojis.is_empty();

        // check authenticity of the command issuer
        if !has_bugbot_emoji {
            // skip commands from random people (no emoji and not staff)
            if !is_staff_user {
                return Ok(());
            }

            // ACK the command with an emoji for the future
            let url = match plan.state {
                PlanState::Issue(_) =>
                    format!(
                        "https://gitlab.archlinux.org/api/v4/projects/{}/issues/{}/notes/{}/award_emoji?name=robot",
                        project.id, plan.iid, note.id),
                PlanState::MergeRequest(_) =>
                    format!(
                        "https://gitlab.archlinux.org/api/v4/projects/{}/merge_requests/{}/notes/{}/award_emoji?name=robot",
                        project.id, plan.iid, note.id),
            };

            self.gitlab_api_post::<AwardEmoji>(&url).await?;
        }

        // decompose tokens
        let command = token[1];
        let params = &token[2..];

        match command {
            "assign" => {
                let note_assignees: Vec<_> = Self::strip_at_sign_from_usernames(params);
                debug!("ADDITIONAL: {:?}", note_assignees);
                plan.assignees.extend(note_assignees);
            }
            "unassign" => {
                let note_assignees: Vec<_> = Self::strip_at_sign_from_usernames(params);
                debug!("REMOVAL: {:?}", note_assignees);
                plan.assignees.retain(|s| !note_assignees.contains(s));
            }
            _ => return Ok(()),
        }

        debug!("note: {:?}", note);
        info!(
            "note: {} {}: {}",
            project.name, note.author.username, note.body
        );

        Ok(())
    }

    pub async fn process_package_maintainers(
        &self,
        plan: &mut Plan,
        project: &GroupProjects,
    ) -> Result<()> {
        if let Some(package_maintainers) = self.data.read().await.maintainers.get(&project.name) {
            let gitlab_maintainers: Vec<_> =
                self.gitlab_users_from_usernames(package_maintainers).await;

            let lock = self.data.read().await;
            let usernames: Vec<_> = self
                .gitlab_username_from_gitlab_users(gitlab_maintainers)
                .into_iter()
                .filter(|username| {
                    // only assign package maintainers to confirmed issues or on their desire
                    !plan.is_unconfirmed || lock.unconfirmed_active.contains(username)
                })
                .collect();
            drop(lock);

            plan.assignees.extend(usernames);
        }

        Ok(())
    }

    pub async fn process_merge_request_package_maintainers(
        &self,
        plan: &mut Plan,
        project: &GroupProjects,
    ) -> Result<()> {
        if let Some(package_maintainers) = self.data.read().await.maintainers.get(&project.name) {
            let gitlab_maintainers: Vec<_> =
                self.gitlab_users_from_usernames(package_maintainers).await;

            let usernames: Vec<_> = self
                .gitlab_username_from_gitlab_users(gitlab_maintainers)
                .into_iter()
                .collect();

            plan.assignees.extend(usernames);
        }

        Ok(())
    }

    pub async fn execute_plan(
        &self,
        plan: &Plan,
        project: &GroupProjects,
        issue: &ProjectIssue,
    ) -> Result<()> {
        let mut endpoint = gitlab::api::projects::issues::EditIssue::builder();

        endpoint
            .project(project.path_with_namespace.to_string())
            .issue(issue.iid);

        for label in &plan.labels {
            endpoint.add_label(label);
        }

        info!(
            "using new assignees: {:?} for issue {} of project {}",
            plan.assignees, issue.iid, project.id
        );
        let users = self.gitlab_users_from_usernames(&plan.assignees).await;
        let new_assignees = self.gitlab_user_ids_from_gitlab_users(&users);

        if new_assignees.is_empty() {
            endpoint.unassign();
        } else {
            endpoint.assignee_ids(new_assignees.into_iter());
        }

        gitlab::api::ignore(endpoint.build()?)
            .query_async(&self.handle.client)
            .await?;

        Ok(())
    }

    pub async fn maybe_execute_plan(
        &self,
        plan: &Plan,
        project: &GroupProjects,
        issue: &ProjectIssue,
    ) -> Result<()> {
        let current_assignees: Vec<u64> = self.gitlab_user_ids_from_gitlab_users(&issue.assignees);
        let new_assignees = self.gitlab_users_from_usernames(&plan.assignees).await;
        let new_assignee_ids: Vec<u64> = self.gitlab_user_ids_from_gitlab_users(&new_assignees);

        debug!(
            "{} {} new assignee ids: {:?}, current: {:?}",
            project.name, issue.title, new_assignee_ids, current_assignees
        );

        let assignees_changed = !current_assignees
            .iter()
            .all(|v| new_assignee_ids.contains(v))
            || !new_assignee_ids
                .iter()
                .all(|v| current_assignees.contains(v));

        if !plan.labels.is_empty() || assignees_changed {
            self.execute_plan(plan, project, issue).await?;
        }

        Ok(())
    }

    pub async fn process_merge_request_labels(
        &self,
        plan: &mut Plan,
        _project: &GroupProjects,
        merge_request: &ProjectMergeRequest,
    ) -> Result<()> {
        // assign unconfirmed status
        if self.merge_request_status_is_unconfirmed(merge_request) {
            plan.is_unconfirmed = true;
        }

        self.assign_default_merge_request_label(plan, merge_request, LabelScope::Status)
            .await?;
        self.assign_default_merge_request_label(plan, merge_request, LabelScope::Priority)
            .await?;

        Ok(())
    }

    pub async fn assign_default_merge_request_label(
        &self,
        plan: &mut Plan,
        merge_request: &ProjectMergeRequest,
        scope: LabelScope,
    ) -> Result<()> {
        // do nothing if issue already has a label of this scope
        if self.merge_request_has_label_scope(merge_request, scope) {
            return Ok(());
        }

        info!(
            "merge request {} has no {:?} label assigned, assigning {}",
            merge_request.title,
            scope,
            scope.default(),
        );
        plan.labels.insert(scope.default().to_string());

        Ok(())
    }

    pub async fn process_merge_request_notes<'a>(
        &'a self,
        plan: &mut Plan,
        project: &GroupProjects,
        merge_request: &ProjectMergeRequest,
    ) -> Result<()> {
        let endpoint = gitlab::api::projects::merge_requests::notes::MergeRequestNotes::builder()
            .project(project.id)
            .merge_request(merge_request.iid)
            .order_by(NoteOrderBy::CreatedAt)
            .sort(SortOrder::Ascending)
            .build()
            .unwrap();
        let notes: Vec<Note> = gitlab::api::paged(endpoint, gitlab::api::Pagination::All)
            .query_async(&self.handle.client)
            .await?;

        for note in notes {
            self.process_note(plan, project, &note).await?;
        }

        Ok(())
    }

    pub async fn maybe_execute_merge_request_plan(
        &self,
        plan: &Plan,
        project: &GroupProjects,
        merge_request: &ProjectMergeRequest,
    ) -> Result<()> {
        let current_assignees: Vec<u64> =
            self.gitlab_user_ids_from_gitlab_users(&merge_request.assignees);
        let new_assignees = self.gitlab_users_from_usernames(&plan.assignees).await;
        let new_assignee_ids: Vec<u64> = self.gitlab_user_ids_from_gitlab_users(&new_assignees);

        debug!(
            "{} {} new assignee ids: {:?}, current: {:?}",
            project.name, merge_request.title, new_assignee_ids, current_assignees
        );

        let assignees_changed = !current_assignees
            .iter()
            .all(|v| new_assignee_ids.contains(v))
            || !new_assignee_ids
                .iter()
                .all(|v| current_assignees.contains(v));

        if !plan.labels.is_empty() || assignees_changed {
            self.execute_merge_request_plan(plan, project, merge_request)
                .await?;
        }

        Ok(())
    }

    pub async fn execute_merge_request_plan(
        &self,
        plan: &Plan,
        project: &GroupProjects,
        merge_request: &ProjectMergeRequest,
    ) -> Result<()> {
        let mut endpoint = gitlab::api::projects::merge_requests::EditMergeRequest::builder();

        endpoint
            .project(project.path_with_namespace.to_string())
            .merge_request(merge_request.iid);

        for label in &plan.labels {
            endpoint.add_label(label);
        }

        info!(
            "using new assignees: {:?} for merge_request {} of project {}",
            plan.assignees, merge_request.iid, project.id
        );
        let users = self.gitlab_users_from_usernames(&plan.assignees).await;
        let new_assignees = self.gitlab_user_ids_from_gitlab_users(&users);

        if new_assignees.is_empty() {
            endpoint.unassigned();
        } else {
            endpoint.assignees(new_assignees.into_iter());
        }

        gitlab::api::ignore(endpoint.build()?)
            .query_async(&self.handle.client)
            .await?;

        Ok(())
    }
}
