use anyhow::Result;
use bugbuddy::args::{Args, Command};
use bugbuddy::BugBuddy;
use clap::Parser;
use env_logger::Env;
use log::error;
use std::sync::Arc;

async fn run(args: Args) -> Result<()> {
    let bot = BugBuddy::new(args.clone()).await?;
    let bot = Arc::new(bot);

    match args.command {
        Command::Run => {
            bot.run(&bot).await?;
        }
        Command::Daemon(daemon_args) => {
            let daemon = bugbuddy::daemon::Daemon::new(daemon_args, bot).await?;
            daemon.start().await?;
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    let logging = match args.verbose {
        0 => "info",
        1 => "bugbuddy=debug",
        2 => "debug",
        _ => "trace",
    };

    env_logger::init_from_env(Env::default().default_filter_or(logging));

    if let Err(err) = run(args).await {
        error!("Error: {:?}", err);
        for cause in err.chain() {
            error!("Caused by: {:?}", cause)
        }
        std::process::exit(1)
    }
}
