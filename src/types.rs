use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use strum_macros::{EnumString, IntoStaticStr};

use gitlab::api::projects::{FeatureAccessLevel, MergeMethod};
use time::OffsetDateTime;

pub type Packager = String;
pub type Pkgbase = String;
pub type PkgbaseMaintainers = HashMap<Pkgbase, Vec<Packager>>;
pub type GitLabUsers = HashMap<Packager, GitLabUser>;

pub struct Plan {
    pub iid: u64,
    pub labels: HashSet<String>,
    pub assignees: HashSet<String>,
    pub is_unconfirmed: bool,
    pub is_security: bool,
    pub state: PlanState,
}

impl Plan {
    pub fn new(iid: u64, state: PlanState) -> Plan {
        Self {
            iid,
            labels: Default::default(),
            assignees: Default::default(),
            is_unconfirmed: false,
            is_security: false,
            state,
        }
    }
}

pub enum PlanState {
    Issue(IssueState),
    MergeRequest(MergeRequestState),
}

#[derive(Debug, Clone, Deserialize)]
pub struct GroupProjects {
    pub id: u64,
    pub name: String,
    pub name_with_namespace: String,
    pub path: String,
    pub path_with_namespace: String,
    pub visibility: ProjectVisibilityLevel,
    pub archived: bool,
    pub request_access_enabled: bool,
    pub lfs_enabled: bool,
    pub issues_access_level: ProjectFeatureAccessLevel,
    pub merge_requests_access_level: ProjectFeatureAccessLevel,
    pub merge_method: ProjectMergeMethod,
    pub only_allow_merge_if_all_discussions_are_resolved: bool,
    pub builds_access_level: ProjectFeatureAccessLevel,
    pub releases_access_level: ProjectFeatureAccessLevel,
    pub container_registry_access_level: ProjectFeatureAccessLevel,
    pub packages_enabled: bool,
    pub snippets_access_level: ProjectFeatureAccessLevel,
}

#[derive(Debug, Deserialize)]
pub struct ProjectIssue {
    pub id: u64,
    pub iid: u64,
    pub project_id: u64,
    pub labels: Vec<String>,
    pub upvotes: u64,
    pub downvotes: u64,
    pub title: String,
    pub assignees: Vec<GitLabUser>,
    pub state: IssueState,
    #[serde(with = "time::serde::iso8601")]
    pub created_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601")]
    pub updated_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601::option", default)]
    pub closed_at: Option<OffsetDateTime>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum IssueState {
    /// The issue is opened.
    Opened,
    /// The issue is closed.
    Closed,
}

#[derive(Debug, Deserialize)]
pub struct ProjectMergeRequest {
    pub id: u64,
    pub iid: u64,
    pub project_id: u64,
    pub labels: Vec<String>,
    pub upvotes: u64,
    pub downvotes: u64,
    pub title: String,
    pub assignees: Vec<GitLabUser>,
    pub reviewers: Vec<GitLabUser>,
    pub state: MergeRequestState,
    pub has_conflicts: bool,
    pub draft: bool,
    #[serde(with = "time::serde::iso8601")]
    pub created_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601")]
    pub updated_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601::option", default)]
    pub closed_at: Option<OffsetDateTime>,
    #[serde(with = "time::serde::iso8601::option", default)]
    pub merged_at: Option<OffsetDateTime>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum MergeRequestState {
    /// The merge request is opened.
    Opened,
    /// The merge request is closed.
    Closed,
    /// The merge request is merged.
    Merged,
    /// The merge request is locked.
    Locked,
}

#[derive(Debug, Deserialize, Clone)]
pub struct GitLabUser {
    pub id: u64,
    pub username: String,
    pub name: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ProjectVisibilityLevel {
    /// The project is visible to anonymous users.
    Public,
    /// The project is visible to logged in users.
    Internal,
    /// The project is visible only to users with explicit access.
    Private,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ProjectFeatureAccessLevel {
    /// The feature is not available at all.
    Disabled,
    /// The features is only available to project members.
    Private,
    /// The feature is available to everyone with access to the project.
    Enabled,
}

impl ProjectFeatureAccessLevel {
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Disabled => "disabled",
            Self::Private => "private",
            Self::Enabled => "enabled",
        }
    }

    pub fn as_gitlab_type(self) -> FeatureAccessLevel {
        match self {
            Self::Disabled => FeatureAccessLevel::Disabled,
            Self::Private => FeatureAccessLevel::Private,
            Self::Enabled => FeatureAccessLevel::Enabled,
        }
    }
}

/// How merge requests should be merged when using the "Merge" button.
#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ProjectMergeMethod {
    /// Always create a merge commit.
    Merge,
    /// Always create a merge commit, but require that the branch be fast-forward capable.
    RebaseMerge,
    /// Only fast-forward merges are allowed.
    #[serde(rename = "ff")]
    FastForward,
}

impl ProjectMergeMethod {
    /// The variable type query parameter.
    pub fn as_str(self) -> &'static str {
        match self {
            Self::Merge => "merge",
            Self::RebaseMerge => "rebase_merge",
            Self::FastForward => "ff",
        }
    }

    pub fn as_gitlab_type(self) -> MergeMethod {
        match self {
            ProjectMergeMethod::Merge => MergeMethod::Merge,
            ProjectMergeMethod::RebaseMerge => MergeMethod::RebaseMerge,
            ProjectMergeMethod::FastForward => MergeMethod::FastForward,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct Note {
    pub id: u64,
    pub project_id: u64,
    pub body: String,
    pub author: GitLabUser,
    #[serde(with = "time::serde::iso8601")]
    pub created_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601")]
    pub updated_at: OffsetDateTime,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[serde(rename_all = "snake_case")]
#[strum(serialize_all = "snake_case")]
pub enum LabelScope {
    Priority,
    Resolution,
    Severity,
    Status,
    Scope,
}

impl LabelScope {
    pub fn default(&self) -> &'static str {
        match self {
            LabelScope::Priority => PriorityLabel::Normal.as_label_name(),
            LabelScope::Resolution => ResolutionLabel::Completed.as_label_name(),
            LabelScope::Severity => SeverityLabel::Low.as_label_name(),
            LabelScope::Status => StatusLabel::Unconfirmed.as_label_name(),
            LabelScope::Scope => ScopeLabel::Bug.as_label_name(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[strum(serialize_all = "kebab-case")]
pub enum PriorityLabel {
    Urgent,
    High,
    Normal,
    Low,
}

impl PriorityLabel {
    pub fn as_label_name(&self) -> &'static str {
        match self {
            Self::Urgent => "priority::1-urgent",
            Self::High => "priority::2-high",
            Self::Normal => "priority::3-normal",
            Self::Low => "priority::4-low",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[strum(serialize_all = "kebab-case")]
pub enum SeverityLabel {
    Critical,
    High,
    Medium,
    Low,
    Lowest,
}

impl SeverityLabel {
    pub fn as_label_name(&self) -> &'static str {
        match self {
            Self::Critical => "severity::1-critical",
            Self::High => "severity::2-high",
            Self::Medium => "severity::3-medium",
            Self::Low => "severity::4-low",
            Self::Lowest => "severity::5-lowest",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[strum(serialize_all = "kebab-case")]
pub enum ScopeLabel {
    Bug,
    Feature,
    Security,
    Question,
    Regression,
    Enhancement,
    Documentation,
    Reproducibility,
}

impl ScopeLabel {
    pub fn as_label_name(&self) -> &'static str {
        match self {
            Self::Bug => "scope::bug",
            Self::Feature => "scope::feature",
            Self::Security => "scope::security",
            Self::Question => "scope::question",
            Self::Regression => "scope::regression",
            Self::Enhancement => "scope::enhancement",
            Self::Documentation => "scope::documentation",
            Self::Reproducibility => "scope::reproducibility",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[strum(serialize_all = "kebab-case")]
pub enum StatusLabel {
    Unconfirmed,
    Confirmed,
}

impl StatusLabel {
    pub fn as_label_name(&self) -> &'static str {
        match self {
            Self::Unconfirmed => "status::unconfirmed",
            Self::Confirmed => "status::confirmed",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumString, Deserialize, IntoStaticStr)]
#[strum(serialize_all = "kebab-case")]
pub enum ResolutionLabel {
    Completed,
    CantReproduce,
    Duplicate,
    NotABug,
    Upstream,
    WontFix,
}

impl ResolutionLabel {
    pub fn as_label_name(&self) -> &'static str {
        match self {
            Self::Completed => "resolution::completed",
            Self::CantReproduce => "resolution::cant-reproduce",
            Self::Duplicate => "resolution::duplicate",
            Self::NotABug => "resolution::not-a-bug",
            Self::Upstream => "resolution::upstream",
            Self::WontFix => "resolution::wont-fix",
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct AwardEmoji {
    pub id: u64,
    pub name: String,
    pub user: GitLabUser,
    #[serde(with = "time::serde::iso8601")]
    pub created_at: OffsetDateTime,
    #[serde(with = "time::serde::iso8601")]
    pub updated_at: OffsetDateTime,
}
